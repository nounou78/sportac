<?php

namespace App\Entity;

use App\Repository\SessionSportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SessionSportRepository::class)]
class SessionSport
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $start_date = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $end_date = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $created_at = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $updated_at = null;

    #[ORM\ManyToOne(inversedBy: 'sessionSports')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Program $program = null;

    #[ORM\OneToMany(mappedBy: 'sessionSport', targetEntity: ExerciceSessionSport::class)]
    private Collection $exerciceSessionSports;

    public function __construct()
    {
        $this->exerciceSessionSports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): static
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): static
    {
        $this->end_date = $end_date;

        return $this;
    }

    // public function getCreatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->created_at;
    // }

    // public function setCreatedAt(\DateTimeImmutable $created_at): static
    // {
    //     $this->created_at = $created_at;

    //     return $this;
    // }

    // public function getUpdatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->updated_at;
    // }

    // public function setUpdatedAt(\DateTimeImmutable $updated_at): static
    // {
    //     $this->updated_at = $updated_at;

    //     return $this;
    // }

    public function getProgram(): ?Program
    {
        return $this->program;
    }

    public function setProgram(?Program $program): static
    {
        $this->program = $program;

        return $this;
    }

    /**
     * @return Collection<int, ExerciceSessionSport>
     */
    public function getExerciceSessionSports(): Collection
    {
        return $this->exerciceSessionSports;
    }

    public function addExerciceSessionSport(ExerciceSessionSport $exerciceSessionSport): static
    {
        if (!$this->exerciceSessionSports->contains($exerciceSessionSport)) {
            $this->exerciceSessionSports->add($exerciceSessionSport);
            $exerciceSessionSport->setSessionSport($this);
        }

        return $this;
    }

    public function removeExerciceSessionSport(ExerciceSessionSport $exerciceSessionSport): static
    {
        if ($this->exerciceSessionSports->removeElement($exerciceSessionSport)) {
            // set the owning side to null (unless already changed)
            if ($exerciceSessionSport->getSessionSport() === $this) {
                $exerciceSessionSport->setSessionSport(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getStartDate() . ' ' . $this->getEndDate();
    }
}
