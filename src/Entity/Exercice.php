<?php

namespace App\Entity;

use App\Repository\ExerciceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\ExerciceType;


#[ORM\Entity(repositoryClass: ExerciceRepository::class)]
class Exercice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $description = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $created_at = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $updated_at = null;

    #[ORM\ManyToOne(inversedBy: 'exercices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ExerciceType $exerciceType = null;

    #[ORM\OneToMany(mappedBy: 'exercice', targetEntity: ExerciceProgram::class)]
    private Collection $exercicePrograms;

    public function __construct()
    {
        $this->exercicePrograms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    // public function getCreatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->created_at;
    // }

    // public function setCreatedAt(\DateTimeImmutable $created_at): static
    // {
    //     $this->created_at = $created_at;

    //     return $this;
    // }

    // public function getUpdatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->updated_at;
    // }

    // public function setUpdatedAt(\DateTimeImmutable $updated_at): static
    // {
    //     $this->updated_at = $updated_at;

    //     return $this;
    // }

    public function getExerciceType(): ?ExerciceType
    {
        return $this->exerciceType;
    }

    public function setExerciceType(?ExerciceType $exerciceType): static
    {
        $this->exerciceType = $exerciceType;

        return $this;
    }

    /**
     * @return Collection<int, ExerciceProgram>
     */
    public function getExercicePrograms(): Collection
    {
        return $this->exercicePrograms;
    }

    public function addExerciceProgram(ExerciceProgram $exerciceProgram): static
    {
        if (!$this->exercicePrograms->contains($exerciceProgram)) {
            $this->exercicePrograms->add($exerciceProgram);
            $exerciceProgram->setExercice($this);
        }

        return $this;
    }

    public function removeExerciceProgram(ExerciceProgram $exerciceProgram): static
    {
        if ($this->exercicePrograms->removeElement($exerciceProgram)) {
            // set the owning side to null (unless already changed)
            if ($exerciceProgram->getExercice() === $this) {
                $exerciceProgram->setExercice(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName() . ' ' . $this->getExerciceType();
    }
}
