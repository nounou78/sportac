<?php

namespace App\Entity;

use App\Entity\ExerciceType;
use App\Entity\Categorie;
use App\Entity\Exercice;
use App\Repository\ProgramRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProgramRepository::class)]
class Program
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $created_at = null;

    // #[ORM\Column]
    // private ?\DateTimeImmutable $updated_at = null;

    #[ORM\ManyToOne(inversedBy: 'programs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\ManyToOne(targetEntity: Categorie::class, inversedBy: 'programs', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Categorie $categorie = null;

    #[ORM\OneToMany(mappedBy: 'program', targetEntity: ExerciceProgram::class)]
    private Collection $exercicePrograms;

    #[ORM\OneToMany(mappedBy: 'program', targetEntity: SessionSport::class)]
    private Collection $sessionSports;

    public function __construct()
    {
        $this->exercicePrograms = new ArrayCollection();
        $this->sessionSports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }
    /*************/
    // public function getCreatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->created_at;
    // }

    // public function setCreatedAt(\DateTimeImmutable $created_at): static
    // {
    //     $this->created_at = $created_at;

    //     return $this;
    // }

    // public function getUpdatedAt(): ?\DateTimeImmutable
    // {
    //     return $this->updated_at;
    // }

    // public function setUpdatedAt(\DateTimeImmutable $updated_at): static
    // {
    //     $this->updated_at = $updated_at;

    //     return $this;
    // }
    /*************/
    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): static
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection<int, ExerciceProgram>
     */
    public function getExercicePrograms(): Collection
    {
        return $this->exercicePrograms;
    }

    public function addExerciceProgram(ExerciceProgram $exerciceProgram): static
    {
        if (!$this->exercicePrograms->contains($exerciceProgram)) {
            $this->exercicePrograms->add($exerciceProgram);
            $exerciceProgram->setProgram($this);
        }

        return $this;
    }

    public function removeExerciceProgram(ExerciceProgram $exerciceProgram): static
    {
        if ($this->exercicePrograms->removeElement($exerciceProgram)) {
            // set the owning side to null (unless already changed)
            if ($exerciceProgram->getProgram() === $this) {
                $exerciceProgram->setProgram(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SessionSport>
     */
    public function getSessionSports(): Collection
    {
        return $this->sessionSports;
    }

    public function addSessionSport(SessionSport $sessionSport): static
    {
        if (!$this->sessionSports->contains($sessionSport)) {
            $this->sessionSports->add($sessionSport);
            $sessionSport->setProgram($this);
        }

        return $this;
    }

    public function removeSessionSport(SessionSport $sessionSport): static
    {
        if ($this->sessionSports->removeElement($sessionSport)) {
            // set the owning side to null (unless already changed)
            if ($sessionSport->getProgram() === $this) {
                $sessionSport->setProgram(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getName();
    }
}
