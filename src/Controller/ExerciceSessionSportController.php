<?php

namespace App\Controller;

use App\Entity\ExerciceSessionSport;
use App\Form\ExerciceSessionSportType;
use App\Repository\ExerciceSessionSportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/session/sport')]
class ExerciceSessionSportController extends AbstractController
{
    #[Route('/', name: 'app_exercice_session_sport_index', methods: ['GET'])]
    public function index(ExerciceSessionSportRepository $exerciceSessionSportRepository): Response
    {
        return $this->render('exercice_session_sport/index.html.twig', [
            'exercice_session_sports' => $exerciceSessionSportRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_session_sport_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ExerciceSessionSportRepository $exerciceSessionSportRepository): Response
    {
        $exerciceSessionSport = new ExerciceSessionSport();
        $form = $this->createForm(ExerciceSessionSportType::class, $exerciceSessionSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceSessionSportRepository->save($exerciceSessionSport, true);

            return $this->redirectToRoute('app_exercice_session_sport_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_session_sport/new.html.twig', [
            'exercice_session_sport' => $exerciceSessionSport,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_session_sport_show', methods: ['GET'])]
    public function show(ExerciceSessionSport $exerciceSessionSport): Response
    {
        return $this->render('exercice_session_sport/show.html.twig', [
            'exercice_session_sport' => $exerciceSessionSport,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_session_sport_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ExerciceSessionSport $exerciceSessionSport, ExerciceSessionSportRepository $exerciceSessionSportRepository): Response
    {
        $form = $this->createForm(ExerciceSessionSportType::class, $exerciceSessionSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceSessionSportRepository->save($exerciceSessionSport, true);

            return $this->redirectToRoute('app_exercice_session_sport_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_session_sport/edit.html.twig', [
            'exercice_session_sport' => $exerciceSessionSport,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_session_sport_delete', methods: ['POST'])]
    public function delete(Request $request, ExerciceSessionSport $exerciceSessionSport, ExerciceSessionSportRepository $exerciceSessionSportRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exerciceSessionSport->getId(), $request->request->get('_token'))) {
            $exerciceSessionSportRepository->remove($exerciceSessionSport, true);
        }

        return $this->redirectToRoute('app_exercice_session_sport_index', [], Response::HTTP_SEE_OTHER);
    }
}
