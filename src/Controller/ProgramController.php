<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\User;
use App\Entity\Program;
use App\Entity\Exercice;
use App\Entity\ExerciceType;
use App\Entity\ExerciceProgram;
use App\Form\ExerciceTypeType;
use App\Form\ProgramType;
use App\Form\ExerciceProgramType;
use App\Form\ExerciceType as TypeExo;
use App\Repository\ProgramRepository;
use App\Repository\ExerciceRepository;
use App\Repository\ExerciceTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/program')]
class ProgramController extends AbstractController
{
    #[Route('/', name: 'app_program_index', methods: ['GET'])]
    public function index(ProgramRepository $programRepository): Response
    {
        return $this->render('program/index.html.twig', [
            'programs' => $programRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_program_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProgramRepository $programRepository, #[CurrentUser] ?User $user): Response
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        // var_dump($user);
        $program = new Program();
        $exercice = new Exercice();
        // $exerciceProgram = new ExerciceProgram();
        $exerciceType = new ExerciceType();
        $form = $this->createForm(ProgramType::class, $program);
        // $form2 = $this->createForm(ExerciceProgramType::class, $exerciceProgram);
        $form2 = $this->createForm(TypeExo::class, $exercice);
        $form3 = $this->createForm(ExerciceTypeType::class, $exerciceType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $program->setUser($user);
            $programRepository->save($program, true);
            return $this->redirectToRoute('app_program_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('program/new.html.twig', [
            'program' => $program,
            'form' => $form,
            'form2' => $form2,
            'form3' => $form3,
        ]);
    }
    /////////////////////////////////////////////
    #[Route('/program/user', name: 'app_program_user', methods: ['GET'])]
    public function programByUserId(ProgramRepository $programRepository): Response
    {
        if ($this->getUser()) {

            $user = $this->getUser();
        }
        return $this->render('program/programUser.html.twig', [
            'programs' => $programRepository->findByUser($user),
        ]);
    }

    // #[Route('/program/exercices', name: 'app_program_exercices', methods: ['GET'])]
    // public function exerciceProgram(ProgramRepository $programRepository): Response
    // {
    //     if ($this->getProgram()) {

    //         $program = $this->getProgram();
    //     }
    //     return $this->render('program/programUser.html.twig', [
    //         'programs' => $programRepository->findByUser($program),
    //     ]);
    // }


    #[Route('/nouveau', name: 'app_program_nouveau', methods: ['GET', 'POST'])]
    public function creerProgram(Request $request, ProgramRepository $programRepository, Session $session)
    {
        $program = new Program();
        $form = $this->createFormBuilder($program)
            // $form = $this->createForm(ProgramType::class, $program);
            // $form->handleRequest($request);
            ->add('name', TextType::class, [
                'label' => 'Nom du programme',
                'attr' => [
                    'placeholder' => "Donnez un nom a votre programme"
                ]
            ])
            ->add('categorie', EntityType::class)
            ->add('Valider', SubmitType::class)
            ->add('Annuler', ResetType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session->set('program', $program);
            // $programRepository->save($program, true);


            return $this->redirectToRoute('app_exercice_new', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('program/nouveau.html.twig', [
            'program' => $program,
            'form' => $form,
        ]);
    }
    /////////////////////////////////////////////

    #[Route('/{id}', name: 'app_program_show', methods: ['GET'])]
    public function show(Program $program): Response
    {
        return $this->render('program/show.html.twig', [
            'program' => $program,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_program_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Program $program, ProgramRepository $programRepository): Response
    {
        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $programRepository->save($program, true);

            return $this->redirectToRoute('app_program_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('program/edit.html.twig', [
            'program' => $program,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_program_delete', methods: ['POST'])]
    public function delete(Request $request, Program $program, ProgramRepository $programRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $program->getId(), $request->request->get('_token'))) {
            $programRepository->remove($program, true);
        }

        return $this->redirectToRoute('app_program_index', [], Response::HTTP_SEE_OTHER);
    }
}
