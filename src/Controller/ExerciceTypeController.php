<?php

namespace App\Controller;

use App\Entity\ExerciceType;
use App\Form\ExerciceTypeType;
use App\Repository\ExerciceTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/type')]
class ExerciceTypeController extends AbstractController
{
    #[Route('/', name: 'app_exercice_type_index', methods: ['GET'])]
    public function index(ExerciceTypeRepository $exerciceTypeRepository): Response
    {
        return $this->render('exercice_type/index.html.twig', [
            'exercice_types' => $exerciceTypeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_type_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ExerciceTypeRepository $exerciceTypeRepository): Response
    {
        $exerciceType = new ExerciceType();
        $form = $this->createForm(ExerciceTypeType::class, $exerciceType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceTypeRepository->save($exerciceType, true);

            return $this->redirectToRoute('app_exercice_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_type/new.html.twig', [
            'exercice_type' => $exerciceType,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_type_show', methods: ['GET'])]
    public function show(ExerciceType $exerciceType): Response
    {
        return $this->render('exercice_type/show.html.twig', [
            'exercice_type' => $exerciceType,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_type_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ExerciceType $exerciceType, ExerciceTypeRepository $exerciceTypeRepository): Response
    {
        $form = $this->createForm(ExerciceTypeType::class, $exerciceType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceTypeRepository->save($exerciceType, true);

            return $this->redirectToRoute('app_exercice_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_type/edit.html.twig', [
            'exercice_type' => $exerciceType,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_type_delete', methods: ['POST'])]
    public function delete(Request $request, ExerciceType $exerciceType, ExerciceTypeRepository $exerciceTypeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exerciceType->getId(), $request->request->get('_token'))) {
            $exerciceTypeRepository->remove($exerciceType, true);
        }

        return $this->redirectToRoute('app_exercice_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
