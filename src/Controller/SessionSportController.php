<?php

namespace App\Controller;

use App\Entity\SessionSport;
use App\Form\SessionSportType;
use App\Repository\SessionSportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/session/sport')]
class SessionSportController extends AbstractController
{
    #[Route('/', name: 'app_session_sport_index', methods: ['GET'])]
    public function index(SessionSportRepository $sessionSportRepository): Response
    {
        return $this->render('session_sport/index.html.twig', [
            'session_sports' => $sessionSportRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_session_sport_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SessionSportRepository $sessionSportRepository): Response
    {
        $sessionSport = new SessionSport();
        $form = $this->createForm(SessionSportType::class, $sessionSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sessionSportRepository->save($sessionSport, true);

            return $this->redirectToRoute('app_session_sport_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('session_sport/new.html.twig', [
            'session_sport' => $sessionSport,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_session_sport_show', methods: ['GET'])]
    public function show(SessionSport $sessionSport): Response
    {
        return $this->render('session_sport/show.html.twig', [
            'session_sport' => $sessionSport,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_session_sport_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, SessionSport $sessionSport, SessionSportRepository $sessionSportRepository): Response
    {
        $form = $this->createForm(SessionSportType::class, $sessionSport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sessionSportRepository->save($sessionSport, true);

            return $this->redirectToRoute('app_session_sport_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('session_sport/edit.html.twig', [
            'session_sport' => $sessionSport,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_session_sport_delete', methods: ['POST'])]
    public function delete(Request $request, SessionSport $sessionSport, SessionSportRepository $sessionSportRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sessionSport->getId(), $request->request->get('_token'))) {
            $sessionSportRepository->remove($sessionSport, true);
        }

        return $this->redirectToRoute('app_session_sport_index', [], Response::HTTP_SEE_OTHER);
    }
}
