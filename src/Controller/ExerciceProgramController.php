<?php

namespace App\Controller;

use App\Entity\ExerciceProgram;
use App\Form\ExerciceProgramType;
use App\Repository\ExerciceProgramRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/exercice/program')]
class ExerciceProgramController extends AbstractController
{
    #[Route('/', name: 'app_exercice_program_index', methods: ['GET'])]
    public function index(ExerciceProgramRepository $exerciceProgramRepository): Response
    {
        return $this->render('exercice_program/index.html.twig', [
            'exercice_programs' => $exerciceProgramRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_exercice_program_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ExerciceProgramRepository $exerciceProgramRepository): Response
    {
        $exerciceProgram = new ExerciceProgram();
        $form = $this->createForm(ExerciceProgramType::class, $exerciceProgram);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceProgramRepository->save($exerciceProgram, true);

            return $this->redirectToRoute('app_exercice_program_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_program/new.html.twig', [
            'exercice_program' => $exerciceProgram,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_program_show', methods: ['GET'])]
    public function show(ExerciceProgram $exerciceProgram): Response
    {
        return $this->render('exercice_program/show.html.twig', [
            'exercice_program' => $exerciceProgram,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_exercice_program_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ExerciceProgram $exerciceProgram, ExerciceProgramRepository $exerciceProgramRepository): Response
    {
        $form = $this->createForm(ExerciceProgramType::class, $exerciceProgram);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exerciceProgramRepository->save($exerciceProgram, true);

            return $this->redirectToRoute('app_exercice_program_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('exercice_program/edit.html.twig', [
            'exercice_program' => $exerciceProgram,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_exercice_program_delete', methods: ['POST'])]
    public function delete(Request $request, ExerciceProgram $exerciceProgram, ExerciceProgramRepository $exerciceProgramRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exerciceProgram->getId(), $request->request->get('_token'))) {
            $exerciceProgramRepository->remove($exerciceProgram, true);
        }

        return $this->redirectToRoute('app_exercice_program_index', [], Response::HTTP_SEE_OTHER);
    }
}
