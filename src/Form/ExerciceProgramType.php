<?php

namespace App\Form;

use App\Entity\ExerciceProgram;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExerciceProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('repetitionNumber')
            ->add('setNumber')
            ->add('duration')
            ->add('exercicePosition')
            // ->add('created_at')
            // ->add('updated_at')
            // ->add('program')
            ->add('exercice');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ExerciceProgram::class,
        ]);
    }
}
