<?php

namespace App\Form;

use App\Entity\Program;
use App\Entity\SessionSport;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionSportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('start_date', DateTimeType::class, [
                'label' => 'Date de début',
                'date_widget' => 'single_text'
            ])
            ->add('end_date', DateTimeType::class, [
                'label' => 'Date de fin',
                'date_widget' => 'single_text'
            ])
            // ->add('created_at')
            // ->add('updated_at')
            ->add('program', EntityType::class, [
                'class' => Program::class,
                'choice_label' => 'name',
                'label' => 'Programme',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SessionSport::class,
        ]);
    }
}
