<?php

namespace App\Form;

use App\Entity\Exercice;
use App\Entity\ExerciceType as TypeExo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExerciceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            // ->add('type')
            ->add('description')
            // ->add('created_at')
            // ->add('updated_at')
            ->add('exerciceType', EntityType::class, [
                'class' => TypeExo::class,
                'choice_label' => 'type',
                'empty_data' => null
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Exercice::class,
        ]);
    }
}
