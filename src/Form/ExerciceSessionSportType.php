<?php

namespace App\Form;

use App\Entity\ExerciceSessionSport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExerciceSessionSportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startDate')
            ->add('endDate')
            // ->add('created_at')
            // ->add('updated_at')
            ->add('exercice')
            ->add('sessionSport');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ExerciceSessionSport::class,
        ]);
    }
}
