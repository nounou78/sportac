<?php

namespace App\Repository;

use App\Entity\ExerciceSessionSport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ExerciceSessionSport>
 *
 * @method ExerciceSessionSport|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExerciceSessionSport|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExerciceSessionSport[]    findAll()
 * @method ExerciceSessionSport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExerciceSessionSportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExerciceSessionSport::class);
    }

    public function save(ExerciceSessionSport $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ExerciceSessionSport $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ExerciceSessionSport[] Returns an array of ExerciceSessionSport objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ExerciceSessionSport
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
