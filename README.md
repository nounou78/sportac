
1- Tout d’abord il faut installer le Un serveur local pour exécuter un projet sur  un poste (symfony Cli) avec la gigne de commande suivante :
scoop install symfony-cli

J’ai choisi le répertoire pour stocker mon  projet : C:\MAMP\htdocs dans le terminal de l’éditeur de texte.

2- J’ai vérifié la version de php installée sur mon poste avec la ligne de commande :
php –v
3- pour vérifier si mon ordinateur répond à tous les Exigences j’ai exécuté la ligne de commande suivante :
symfony check:requirements

4- J’ai créé mon projet avec la ligne de commande suivante  car j’ai choisi de travailler avec la dernière version stable LTS:
composer create-project symfony/skeleton:"^5.4"  sportAc

5- Une fois le projet crée, je me suis redirigée vers le répertoire du projet avec la ligne de commande:
cd my_project_directory

6- J’ai commencé à installer les packages nécessaire pour son bon fonctionnement.
composer install
composer require webapp
composer require symfony/orm-pack     (doctrine)
composer require --dev symfony/maker-bundle
symfony server:start
composer require logger

J'ai créer ma base de données:
php bin/console doctrine:database:create

Ces lignes de commandes permettent d’installer les dépendances PHP et JavaScript dans votre projet:
composer require symfony/webpack-encore-bundle
npm install
Une fois ces deux lignes exécutées, un répertoire « Asset » est créé, par la suite  j’ai suivi toutes les instructions d’installation sur le site de symfony.
Renommer le fichier app.css en app.scss et modifier l’import dans le fichier app.js (import ‘./styles/app.scss’)

7- J'ai installé Twig avec : « composer install twig »
8- J’ai commencé à créer les entités avec le maker : php bin/console make :entity
il faut : 
             Donner un nom à l’entité
             ajouter les  tous les champs que j’ai besoin après avoir répondu à quelques questions.

une fois toutes les entités crées, j’ai effectué les changement souhaités.
9- j’ai fait la migration pour générer une fichier de version  :
php bin/console make:migration

10-  J’ai vérifié le fichier de migration avant de persister en BDD :
php bin/console doctrine:migrations:migrate







