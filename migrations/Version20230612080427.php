<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230612080427 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE exercice_session_sport (id INT AUTO_INCREMENT NOT NULL, exercice_id INT NOT NULL, session_sport_id INT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_6587694389D40298 (exercice_id), INDEX IDX_6587694327D4DC90 (session_sport_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exercice_session_sport ADD CONSTRAINT FK_6587694389D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id)');
        $this->addSql('ALTER TABLE exercice_session_sport ADD CONSTRAINT FK_6587694327D4DC90 FOREIGN KEY (session_sport_id) REFERENCES session_sport (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exercice_session_sport DROP FOREIGN KEY FK_6587694389D40298');
        $this->addSql('ALTER TABLE exercice_session_sport DROP FOREIGN KEY FK_6587694327D4DC90');
        $this->addSql('DROP TABLE exercice_session_sport');
    }
}
